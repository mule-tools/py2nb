# py2nb

py2nb is a tool to convert between structured python scripts and
IPython notebooks.

## Why py2nb?

Notebooks are difficult to properly keep under version control and can
sometimes be less convenient to work with. In its [user library](https://gitlab.com/mule-tools/user-library/)
McMule uses a (somewhat) structure form of python scripts that are
converted to IPython notebooks by the CI system for better viewing.

## Structured python scripts
Many text editors allow for code folding as a method of structuring.
For example, [vim](https://vim.wikia.com/wiki/Folding) and [emacs](https://www.emacswiki.org/emacs/FoldingMode)
use a pair of markers `{{{` and `}}}` to indicate folding groups.
Combined with normal comments, this allows for structuring and
documentation that can be converted to a notebook.

## Using py2nb

### script to notebook
To convert from script to notebook, use the tool `py2nb`, for example
```bash
$ py2nb script.py
```
Optional arguments included

 * `-o`: specify the output
 * `-x`: execute notebook before saving
 * `--html`: also save notebook as html file


### notebook to script
py2nb provides a reverse converter using
[nbconvert](https://nbconvert.readthedocs.io/en/latest/) called
`pyvim`. For example,
```bash
$ jupyter nbconvert --to pyvim notebook.ipynb
```
Note that this is not necessarily the exact inverse of `py2nb` and the
output should be scrutinised before committing.
