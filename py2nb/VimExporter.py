import nbconvert
from traitlets import default
import os
import os.path
import re
from .md_tw import TWMarkdown

nbconvert.TemplateExporter.extra_template_basedirs = [
    os.path.join(os.path.dirname(__file__), "templates")
]


class VimExporter(nbconvert.TemplateExporter):
    def __init__(self, *args, **kwargs):
        self.section_indent = 0
        self.environment.globals['footerFunc'] = self.footerFunc
        self.md_render = TWMarkdown()
        self.previous = None
        super(VimExporter, self).__init__(*args, **kwargs)

    """
    Exports a VIM Python code file.
    """
    @default('file_extension')
    def _file_extension_default(self):
        return '.py'

    @property
    def template_path(self):
        """
        We want to inherit from HTML template, and have template under
        ``./templates/`` so append it to the search path. (see next section)

        Note: nbconvert 6.0 changed ``template_path`` to ``template_paths``
        """
        return super().template_path+[
            os.path.join(os.path.dirname(__file__), "templates")
        ]

    @default('template_file')
    def _template_file_default(self):
        if nbconvert.__version__[0] == '5':
            return 'pyvim.tpl'
        else:
            return super()._template_file_default()

    @default('template_name')
    def _template_name_default(self):
        return 'pyvim'

    def default_filters(self):
        for x in super().default_filters():
            yield x
        yield ('vim_comment_lines', self.vim_comment_lines)
        yield ('myipython2python', self.myipython2python)

    def sectionEnd(self, stop=0):
        ans = ""
        while self.section_indent > stop:
            if self.section_indent > 7:
                ans += "#"*9
            else:
                ans += "#" * (79-10*self.section_indent)
            ans += "}}}\n"
            self.section_indent -= 1
        return ans

    def vim_comment_lines(self, text, prefix='# '):
        ans = []
        lines = text.splitlines()
        blob = []

        def finish_blob():
            if not blob:
                return
            self.previous = 'md'
            txt = self.md_render('\n'.join(blob))
            ans.append(prefix + ('\n'+prefix).join(txt.split('\n')) + '\n')

        while lines:
            line = lines.pop(0)
            m = re.match("^(#+) (.*)", line)
            if m:
                finish_blob() ; blob = []

                self.previous = 'head'
                # Is heading
                sharps, header = m.groups()
                sharps = len(sharps)

                ans.append(self.sectionEnd(sharps - 1))
                self.section_indent += 1

                ans.append("#" * (sharps + 1) + " " + header + "{{{\n")
            else:
                blob.append(line)
        finish_blob()
        return ''.join(ans)

    def myipython2python(self, text):
        ans = nbconvert.filters.ipython2python(text)
        if self.previous == 'code':
            ans = '\n\n' + ans
        self.previous = 'code'
        return ans

    def footerFunc(self):
        return self.sectionEnd()

    output_mimetype = 'text/x-python'
