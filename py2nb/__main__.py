from . import py2nb, executenb, nb2html, gitauthors, binderurl
import argparse
import nbformat
import os


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'input', type=argparse.FileType('r'), help='input file'
    )
    parser.add_argument(
        '-o', type=argparse.FileType('w'), help='output file'
    )
    parser.add_argument(
        '-x', action='store_true', help="execute notebook"
    )
    parser.add_argument(
        '--html', action='store_true', help="convert result to html"
    )
    parser.add_argument(
        '--pattern', type=str, default='from pymule import *'
    )

    args = parser.parse_args()

    if args.o is None:
        if args.input.name == '<stdin>':
            name = 'stdin'
        else:
            name = os.path.splitext(args.input.name)[0]
        fp = open(name + ".ipynb", 'w')
    else:
        fp = args.o

    nb = py2nb(args.input.read())
    if args.x:
        print("Executing " + args.input.name)
        dir = os.path.dirname(name)
        if len(dir) == 0:
            dir = "."
        nb = executenb(nb, dir, args.pattern)

    binder = binderurl(fp.name)
    if binder:
        nb.metadata['binderurl'] = binder

    if args.input.name != '<stdin>':
        authors = gitauthors(args.input.name)
        if authors:
            nb.metadata.authors = [{'name': i} for i in authors]

    nbformat.write(nb, fp)

    if args.html:
        html = nb2html(nb)
        with open(name + ".html", 'w') as fp:
            fp.write(html.encode("utf-8"))


if __name__ == '__main__':
    main()
