import nbconvert
import nbformat
import nbformat.v4
import re
import subprocess
import os
import time


def debug(s):
    pass


def gitauthors(file):
    proc = subprocess.Popen([
        'git', 'log', '--pretty=format:%an', file
    ], stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    o, e = proc.communicate()
    if e:
        return []
    else:
        return sorted(list(set(o.decode('utf-8').splitlines())))


def binderurl(file):
    def execGit(cmd):
        proc = subprocess.Popen(
            ['git'] + cmd,
            stderr=subprocess.PIPE, stdout=subprocess.PIPE
        )
        o, e = proc.communicate()
        if e:
            raise ValueError()
        return o.decode('utf-8').strip()

    try:
        commit = execGit(['rev-parse', '--short', 'HEAD'])

        m = re.match(
            '[^@]*@gitlab.com[:/](.*).git',
            execGit(['remote', 'get-url', 'origin'])
        )
        if not m:
            print("Warning: giturl only works for gitlab.com")
            return ""
        url = m.groups()[0]

        root = execGit(['rev-parse', '--show-prefix'])
        filepath = os.path.join(
            root, file
        )

        return "https://mybinder.org/v2/gl/%s/%s?filepath=%s" % (
            url.replace('/', '%2F'),
            commit,
            filepath
        )
    except ValueError:
        return ""


def py2nb(py):
    current = ["", ""]
    indent = 0
    nb = nbformat.v4.new_notebook()

    if 'PY2NB_PATH' in os.environ:
        nb.metadata['py2nb_path'] = os.environ['PY2NB_PATH'].split(':')

    def push_cell():
        debug("[PUSH] current state is %s. Adding %d lines." % (
            current[0], len(current[1].splitlines())
        ))
        if current[0] == "":
            return
        elif current[0] == "comment":
            c = nbformat.v4.new_markdown_cell(
                current[1].rstrip().lstrip()
            )
        elif current[0] == "code":
            c = nbformat.v4.new_code_cell(
                current[1].rstrip().lstrip()
            )
        nb.cells.append(c)
        current[0] = ""
        current[1] = ""

    def add_cell(c, t):
        debug("[ADD ] Adding %d characters as %s" % (len(c), t))
        if current[0] != t:
            push_cell()
        current[0] = t
        current[1] += c + "\n"

    lines = py.splitlines()
    i = -1
    while i < len(lines) - 1:
        i += 1
        line = lines[i]
        if line == "# vim: foldmethod=marker":
            debug("[%4d] Skip vi header" % (i+1))
            continue
        if line.startswith("# py2nb:"):
            key, value = re.match('# py2nb: ([a-z\d]*)=(.*)', line).groups()
            nb.metadata[key] = value
            continue
        if line.endswith("#########}}}"):
            debug("[%4d] Skip section end. Indent was %d" % (i, indent))
            indent -= 1
            push_cell()
            continue
        if len(line) == 0:
            debug("[%4d] empty line..." % (i+1))
            # Empty line, is next empty as well?
            if len(lines[i+1]) == 0:
                debug("[    ] next line empty, too. Push")
                i += 1
                push_cell()
            else:
                debug("[    ] next line not empty. Add")
                add_cell(line, 'code')
            continue
        if line[0] == "#":
            debug("[%4d] Comment line" % (i+1))
            c = line.strip('#').strip()
            if line.endswith("{{{"):
                debug("[    ] Section begin. Indent was %d" % indent)
                indent += 1
                c = "#"*indent + " " + c[:-3]

            add_cell(c, 'comment')
        if line[0] != "#":
            debug("[%4d] Code line" % (i+1))
            add_cell(line, 'code')

    push_cell()
    return nb


def executenb(nb, dir, pymule):
    for i in range(len(nb.cells)):
        if pymule in nb.cells[i]['source']:
            break

    old = nb.cells[i]['source']
    nb.cells[i]['source'] = nb.cells[i]['source'].replace(
        pymule,
        pymule + "\nget_ipython().magic('matplotlib inline')"
    )

    t0 = time.time()
    ep = nbconvert.preprocessors.ExecutePreprocessor(
        timeout=600
    )
    ep.preprocess(nb, {'metadata': {'path': dir}})
    t1 = time.time()

    print("Executing notebook took %fs" % (t1-t0))

    nb.cells[i]['source'] = old
    return nb


def nb2html(nb):
    html_exporter = nbconvert.HTMLExporter()
    (body, resources) = html_exporter.from_notebook_node(nb)
    return body
