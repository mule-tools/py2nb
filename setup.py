import setuptools
from os import path


this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, 'README.md')) as f:
    long_description = f.read()

setuptools.setup(
    name='py2nb',
    version='0.0.1',
    license='GPLv3',
    author='Yannick Ulrich',
    author_email='yannick.ulrich@durham.ac.uk',
    description='A tool to convert python scripts to notebooks',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/mule-tools/py2nb',
    packages=setuptools.find_packages(),
    classifiers=[
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3',
        'Operating System :: OS Independent',
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Framework :: IPython',
        'Framework :: Jupyter',
        'Framework :: Matplotlib',
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering :: Physics'
    ],
    zip_safe = False,
    install_requires=[
        'nbconvert>=5.6.1',
        'nbformat>=5.0.5'
    ],
    entry_points={
        'console_scripts': [
            'py2nb = py2nb.__main__:main'
        ],
        'nbconvert.exporters': [
            'pyvim = py2nb:VimExporter'
        ]
    },
    package_data={'py2nb': [
        'templates/pyvim.tpl',
        'templates/pyvim/conf.json',
        'templates/pyvim/index.py.j2'
    ]},
    include_package_data=True,
    python_requires='>=3'
)
